extern crate core;
extern crate sebastian;

use core::mem;
use std::io;
use std::fs;
use std::io::ErrorKind::*;
use std::io::Read;
use std::io::Write;
use std::os::unix::fs::PermissionsExt;
use std::thread;
use std::time::Duration;
use sebastian::http::Response;

fn executable(file: &fs::File) -> io::Result<bool> {
    let mode = file.metadata()?.permissions().mode();
    Ok(mode & 0111 != 0)
}

fn print_chunk(data: &[u8]) -> io::Result<()> {
    print!("{:X}\r\n", data.len());
    io::stdout().write_all(data)?;
    print!("\r\n");
    Ok(())
}

fn send_last_chunk() -> io::Result<()> {
    print_chunk(b"")
}

fn exhaust(f: &mut fs::File) -> io::Result<()> {
    let mut buffer : [u8; 1024] = unsafe { mem::uninitialized() };
    loop {
        let nbytes = f.read(&mut buffer)?;
        if nbytes == 0 {
            return Ok(());
        }
        print_chunk(&buffer[0..nbytes])?;
    }
}

/* Note [Error after initial header]
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * If we already started transmission (sent response code)
 * we have no means to report error to client; best thing we can do
 * is to send zero-length chunk. If all goes well, client will receive
 * lesser, than expected, but still well-formed response.
 */


fn process_request() -> Result<(), Response<'static>> {
    let bad_request = || Response::new400("Failed to parse request");
    let bad_method  = || Response::new403("Only GET method is permitted");
    let bad_file    = |e : io::Error| match e.kind() {
            NotFound => Response::new404("File not found"),
            _        => Response::new403("I/O error")
        };

    let request = sebastian::io::gets().map_err(|_| bad_request())?;
    let mut it = request.split_whitespace();
    let method = it.next().ok_or_else(bad_request)?;
    if method != "GET" {
        return Err(bad_method());
    }
    let path = it.next().ok_or_else(bad_request)?;
    let mut file = fs::File::open(path).map_err(bad_file)?;
    let meta = file.metadata().map_err(bad_file)?;
    if ! meta.is_file() {
        return Err(Response::new404("Path is not regular file"));
    }

    println!("HTTP/1.1 200 OK");
    println!("Content-Type: text/plain");
    println!("Transfer-Encoding: chunked\n");

    loop {
        if let Err(_) = exhaust(&mut file) { // See [Error after initial header]
            let _ = send_last_chunk();
            return Ok(());
        }
        if executable(&file).expect("stat() must succeed") {
            break
        }
        thread::sleep(Duration::from_millis(500));
    }
    // See [Error after initial header]
    let _ = send_last_chunk();
    Ok(())
}

fn main() {
    if let Err(response) = process_request() {
        response.serve();
    }
}
