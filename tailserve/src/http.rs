extern crate chrono;

macro_rules! response_new {
    ($name:ident, $text:expr) => (
        pub fn $name(body: &str) -> Response {
            let code = stringify!($name)[3..].parse().unwrap();
            let text = $text;
            Response { code, text, body }
        }
    )
}

pub struct Response<'a> {
    code: i32,
    text: &'a str,
    body: &'a str
}


impl<'a> Response<'a> {
    pub fn serve(&self) {
        let now = chrono::prelude::Local::now().to_rfc2822();
        println!("HTTP/1.0 {} {}", self.code, self.text);
        println!("Date: {}", now);
        println!("Server: Black butler");
        println!("Connection: close");
        println!("Content-Length: {}", self.body.len());
        println!("Content-Type: text/plain; charset=utf-8");
        println!("\n{}", self.body);
    }

    response_new!(new400, "Bad Request");
    response_new!(new403, "Forbidden");
    response_new!(new404, "Not Found");
}
