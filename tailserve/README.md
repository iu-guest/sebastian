tailserve -- serve files being updated via HTTP
-----------------------------------------------

Sometimes, you may want to deliver file being updated by network.
Usually, it is some kind of log file, which is being appended right now.
The most prominent example, when it is necessary, is coninuous integration
system, like Jenkins or Laminar, which serve output of command being
executed in real time.

All of them -- Jenkins, GitLab, even Laminar, which claims to be
minimalistic, implement it with pile of JavaScript, sometimes using
even more complicated technologies, like WebSockets.

But actually, HTTP/1.1 already provides standard solution for this kind
of problem -- chunked transfer encoding. Client connects to server, which
serves file chunk by chunk, until it somehow know, that file is not going
to be updated anymore.

There is two conventions I am aware of in regard this situation:

 * runit supervision system makes rotated log file, that will not be
   updated anymore executable.

 * nq queue management system use exclusive flock(2) on ouput file. When
   it is not going to update file anymore, it releases lock.

So what tailserve does right now, it to serve GET http requests by chunks.
File is considered final, when it becomes executable.

Surely, there is downside -- most(all?) browsers refuse to render data
incrementally, so you would need to use some console tool, like curl, wget
or netcat to enjoy the show.

Perfomance consideration
------------------------

tailserve, as implemented right now, is meant to be run by
tcpserver/xinetd, creating one process for every single client request.
Since every such processes is going to be long-living, this solution is
definitely not going to scale. As such, there are plans to change
implementation to handle all clients within limited number of unix
processes.

